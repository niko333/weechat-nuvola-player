nuvola-player
======
``nuvola-player`` is a weechat plugin to interact with a running nuvola player instance

Features
--------
- Share what song you're currently listening to on the current buffer
- Play, pause, skip, etc the current track
- Like/dislike the current song
- Display a desktop notification showing the details of the currently playing song

Installation
------------
Run the ``install.sh`` script

Or

Copy nuvola-player.py to the python directory inside the Weechat directory
(cp nuvola-player.py ~/.weechat/python)
make a softlink to the plugin from autoload 
(cd ~/.weechat/python/autoload; ln -s ../nuvola-player.py nuvola-player.py)


Usage
-----
Run the command with either /music or /nuvola-player from any server or private buffer

Issue
-----
Issue tracker can be found here_

.. _here: https://bitbucket.org/niko333/weechat-nuvola-player/issues?status=new&status=open
