# Niko Oliveira
# The MIT License (MIT)
# Copyright (c) 2017, Nikolas Oliveira
#
import weechat
import subprocess
import re

SCRIPT_NAME = 'nuvola_player'
SCRIPT_AUTHOR = 'Nikolas Oliveira'
SCRIPT_VERSION = '0.9.0'
SCRIPT_LICENSE = 'MIT'
SCRIPT_DESCRIPTION = ('Put a description here')

COMPLETION_LABEL = 'nuvola_player_completion'


def share(buffer):
    """Share the current track you're playing on Nuvola.
    Determines the artist and title and uses the /me irc command to share
    with the rest of the buffer"""
    title = run_command(buffer, ['nuvolaplayer3ctl', 'track-info', 'title'])
    artist = run_command(buffer, ['nuvolaplayer3ctl', 'track-info', 'artist'])

    if not title or not artist:
        weechat.prnt(buffer, 'Could not identify title or artist, '
                     'is Nuvola Player playing any media?')
        return weechat.WEECHAT_RC_ERROR

    weechat.command(buffer, "/me is playing: %s by %s" % (title, artist))

    return weechat.WEECHAT_RC_OK


def run_action(buffer, action):
    """Run <action> with nuvolaplayer3ctl"""
    # Check if the action is available
    if not check_if_action_enabled(buffer, action):
        weechat.prnt(buffer, 'Action, %s, is not currently available'
                     ', or is not a valid Nuvola Player playback command'
                     % action)
        return weechat.WEECHAT_RC_ERROR

    run_command(buffer, ['nuvolaplayer3ctl', 'action', action])

    return weechat.WEECHAT_RC_OK


def check_if_action_enabled(buffer, action):
    """Check if <action> is enabled at the moment.
    E.g. pause action will be disabled until something is actually playing"""
    actions = run_command(buffer, ['nuvolaplayer3ctl', 'list-actions'])

    for line in actions.splitlines():
        match = re.search(r' *  %s \((enabled)\).*' % action.strip(), actions)
        if match:
            return bool(match.groups())

    return False


def run_command(buffer, cmd):
    """Use Popen to run a process capturing it's stdout and stderr"""
    proc = subprocess.Popen(cmd, stdout=subprocess.PIPE,
                            stderr=subprocess.PIPE)
    stdout, stderr = proc.communicate()

    if proc.returncode != 0:
        weechat.prnt('', 'stderr: %s ' % stderr)
        if proc.returncode == 2 and 'Failed to connect to Nuvola' in stderr:
            weechat.prnt_date_tags(buffer, 0, 'no-log',
                                   'failed to connect to Nuvola Player '
                                   'It\'s likely not running')

    return stdout.strip()


def generate_actions():
    """Generate the possible playback commands for Nuvola Player"""
    actions_help = run_command('', ['nuvolaplayer3ctl', 'list-actions'])
    read_actions = False  # Flip this to True once we found the playback block

    # Add a "share" command. This is not a Nuvola Player action. Share is to
    # share what you're currently listening to with your friends on the current
    # irc buffer, using /me
    actions = ['share']

    for line in actions_help.splitlines():
        if not line:
            continue

        if read_actions:
            # Example line:
            # *  play (disabled) - Play
            match = re.search(r' \*  (.*) \(\w+\) - \w+', line)
            if match:
                # We only have one grouping in the above regex
                actions.append(match.groups()[0])
            else:
                # Read actions was true, yet we didn't find a match on this
                # line, therfore we must have exhauseted the playback section
                # of the list-actions output.
                break

        if 'Group: playback' in line:
            # We've found the block of playback actions
            read_actions = True

    return actions


def nuvola_player(data, buffer, args):
    """Main nuvola_player hook"""
    if args:
        # Currently Nuvola-Player only takes single arguments, so only take the
        # first argument
        arg = args.split()[0]

        if arg:
            if arg == 'share':
                # Someone ran share explicitly
                return share(buffer)

            # Otherwise it was a Nuvola actions, so run that action
            return run_action(buffer, arg)

    else:
        # The default action is share if no arguments are provided
        return share(buffer)

    return weechat.WEECHAT_RC_OK


def completion_cb(data, completion_item, buffer, completion):
    actions = generate_actions()
    for action in actions:
        weechat.hook_completion_list_add(completion, action, 0,
                                         weechat.WEECHAT_LIST_POS_SORT)
    return weechat.WEECHAT_RC_OK


# Register the plugin
weechat.register(SCRIPT_NAME, SCRIPT_AUTHOR, SCRIPT_VERSION,
                 SCRIPT_LICENSE, SCRIPT_DESCRIPTION, '', '')

command_help = """Interact with Nuvola Player3 from your Weechat client!

Share what you\'re listening to on the current buffer

Also perform actions (if applicable to the current streaming service) such as:
- Play/pause/skip-song the current song, etc
- Like/dislike the current song
- Send a desktop notification displaying the info for the current song

NOTE:
  Since each streaming service Nuvola Player supports has different set of
  available actions, the arguments to this plugin are computed in real time,
  please use tab completion to see the list of available actions
"""

# Hook the command to both /nuvola-player and /music
for command_hook in ['nuvola-player', 'music']:
    weechat.hook_command(command_hook, command_help,
                         '', '',
                         '%({})'.format(COMPLETION_LABEL),
                         'nuvola_player', '')

# Hook completion to generate the current list of available actions each time
# completion is attempted
weechat.hook_completion(COMPLETION_LABEL,
                        'A custom completion for the Nuvola Player plugin',
                        'completion_cb', '')
